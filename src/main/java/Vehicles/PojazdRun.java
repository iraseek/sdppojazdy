package Vehicles;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class PojazdRun {
    private static final Logger logger = LoggerFactory.getLogger(PojazdRun.class);
    Scanner sc = new Scanner(System.in);

    List<Pojazd> pojazds = new ArrayList<>();

    private Pojazd TheFastest(String type){
        List<Pojazd> lista;
        if(Objects.equals(type, "ALL")){
            lista = pojazds;
        }else{
            lista = allVehicles(type);
        }

        Pojazd fastest = lista.get(0);

        for(int i=1; i < lista.size();i++) {
            if (lista.get(i).getTopSpeed() > fastest.getTopSpeed()){
                fastest = lista.get(i);
            }

        }
        return fastest;
    }

    private List<Pojazd> allVehicles(String type){

        List<Pojazd> drive = new ArrayList<>();
        for(int i =0;i < pojazds.size();i++){
            if (Objects.equals(pojazds.get(i).getTyp(), type)){
                drive.add(pojazds.get(i));
            }
        }
        return drive;
    }



    public void run() {
        pojazds.add(new Pojazd("Mercedes", 300, "CAR"));
        pojazds.add(new Pojazd("AUDI", 290, "CAR"));
        pojazds.add(new Pojazd("Tank", 100, "SHIP"));
        pojazds.add(new Pojazd("WarShip", 100, "SHIP"));
        pojazds.add(new Pojazd("JetPack", 400, "PLANE"));
        pojazds.add(new Pojazd("SpaceShip", 550, "PLANE"));
        pojazds.add(new Pojazd("GRANT", 40, "BICYCLE"));
        pojazds.add(new Pojazd("BMX", 25, "BICYCLE"));


        while (true) {
            System.out.println("CAR, SHIP, PLANE, BICYCLE, ALL, EXIT");
            String word = sc.nextLine();
            word = word.toUpperCase(Locale.ROOT);

            switch (word) {
                case "CAR":
                case "SHIP":
                case "PLANE":
                case "BICYCLE":
                Pojazd pojazd = TheFastest(word);
                logger.info("Pojazd " +pojazd.getTyp() +
                       " producenta " + pojazd.getProducent() +
                        " jest najszybszy (maksymalna prędkość to= " + pojazd.getTopSpeed() + ")");
                    break;
                case "ALL":
                Pojazd pojazd1 = TheFastest(word);
                logger.info("Najszybszy pojazd ze wszystkich to : " + pojazd1.getTyp() +
                        " Producenta: " + pojazd1.getProducent() +
                        " którego prętkość to: " +pojazd1.getTopSpeed());
                    break;
                case "EXIT":
                    System.exit(0);
                    break;
                default:
                    logger.info("Podaj prawidłową opcję");

            }


        }
    }
}
