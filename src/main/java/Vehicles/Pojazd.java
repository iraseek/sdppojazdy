package Vehicles;

public class Pojazd {

        private String producent;
        private int topSpeed;
        private String typ;

public Pojazd(String producent,int topSpeed,String typ) {
this.producent = producent;
this.topSpeed = topSpeed;
this.typ = typ;
}

    public String getProducent() {
        return producent;
    }

    public int getTopSpeed() {
        return topSpeed;
    }

    public String getTyp() {
        return typ;
    }

    public void setProducent(String producent){
        this.producent = producent;
    }

    public void setTopSpeed(int topSpeed){
        this.topSpeed = topSpeed;
    }

    public void setTyp(String typ){
        this.typ = typ;
    }

}
